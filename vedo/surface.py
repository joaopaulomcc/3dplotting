import numpy as np
import vedo as vd


def z_func(x, y):

    return np.sin(2 * x * y) * np.cos(3 * y ) / 2


surf = vd.pyplot.plot(
    z_func,
    xlim=[0, 3],
    ylim=[0, 5],
    bins=[500, 500],
    c="coolwarm",
    zlevels=0,
)

axes_dict = {
    "xtitle": "X Title",
    "ytitle": "Y Title",
    "ztitle": "Z Title",
    "zxGrid": True,
    "xFlipText": False,
}

vd.show(surf)
