import numpy as np
import vedo as vd

N = 10

t = np.linspace(-16 * np.pi, 16 * np.pi, 1000)
x = np.cos(t) * t / 10
y = np.sin(t) * t / 10
z = np.linspace(0, 5, 1000)

color = "firebrick"

a = np.array([x, y, z])
a = a.transpose()

pts_coords = np.random.rand(N, 3)

lines = vd.Line(
    a,
    c=color,
    lw=5,
    res=2,
    alpha=0.75,
)

print(f"Line Length: {lines.length()}")

axes_dict = {
    "xtitle": "X Title",
    "ytitle": "Y Title",
    "ztitle": "Z Title",
    "zxGrid": True,
    "xFlipText": False,
}

vd.show(lines, axes=axes_dict)
