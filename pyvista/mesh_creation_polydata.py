import numpy as np
from numpy.core.shape_base import hstack
import pyvista as pv


# Mesh Points
vertices = np.array([[0, 0, 0],
                     [1, 0, 0],
                     [1, 1, 0],
                     [0, 1, 0],
                     [0.5, 0.5, -1]])

# Mesh faces
faces = np.hstack([[4, 0, 1, 2, 3],
                   [3, 0, 1, 4],
                   [3, 1, 2, 4]])

surf = pv.PolyData(vertices, faces)

# Plot each face with a different color
surf.plot(scalars=np.arange(3), cpos=[-1, 1, 0.5])
