# %%
import numpy as np
import pyvista as pv
from pyvista import examples

# %%
mesh = examples.load_airplane()

# %%
print(f"Mesh Center: {mesh.center}")
print(f"Mesh Points: {mesh.points[0:5, :]}")

# %%
mesh = examples.load_uniform()
arr = mesh.point_arrays["Spatial Point Data"]
mesh.cell_arrays["foo"] = np.random.rand(mesh.n_cells)
foo = mesh["foo"]
mesh["new-array"] = np.random.rand(mesh.n_points)

# %%
mesh = examples.load_airplane()
# mesh.plot()

# %%
plotter = pv.Plotter()
plotter.add_mesh(mesh)
cpos = plotter.show()

# %%
plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(mesh, color="tan")
plotter.camera_position = cpos
plotter.show()
