import pyvista as pv
from math import pi

# Supertoroid
supertoroid = pv.ParametricSuperToroid(n1=0.5)
supertoroid.plot(color="tan", smooth_shading=False)

# Ellipsoid with a long x axis
ellipsoid = pv.ParametricEllipsoid(10, 5, 5)
ellipsoid.plot(color="tan")

# cool plotting direction
cpos = [
    (21.9930, 21.1810, -30.3780),
    (-1.1640, -1.3098, -0.1061),
    (0.8498, -0.2515, 0.4631),
]


# half ellipsoid
part_ellipsoid = pv.ParametricEllipsoid(10, 5, 5, max_v=pi / 2)
part_ellipsoid.plot(color="tan", smooth_shading=True, cpos=cpos)

pseudosphere = pv.ParametricPseudosphere()
pseudosphere.plot(color="tan", smooth_shading=True)

bohemiandome = pv.ParametricBohemianDome()
bohemiandome.plot(color="tan")

bour = pv.ParametricBour()
bour.plot(color="tan")

boy = pv.ParametricBoy()
boy.plot(color="tan")

catalanminimal = pv.ParametricCatalanMinimal()
catalanminimal.plot(color="tan")

conicspiral = pv.ParametricConicSpiral()
conicspiral.plot(color="tan")

crosscap = pv.ParametricCrossCap()
crosscap.plot(color="tan")

dini = pv.ParametricDini()
dini.plot(color="tan")

enneper = pv.ParametricEnneper()
enneper.plot(cpos="yz")

figure8klein = pv.ParametricFigure8Klein()
figure8klein.plot()

henneberg = pv.ParametricHenneberg()
henneberg.plot(color="tan")

klein = pv.ParametricKlein()
klein.plot(color="tan")

kuen = pv.ParametricKuen()
kuen.plot(color="tan")

mobius = pv.ParametricMobius()
mobius.plot(color="tan")

pluckerconoid = pv.ParametricPluckerConoid()
pluckerconoid.plot(color="tan")

randomhills = pv.ParametricRandomHills()
randomhills.plot(color="tan")

roman = pv.ParametricRoman()
roman.plot(color="tan")

superellipsoid = pv.ParametricSuperEllipsoid(n1=0.1, n2=2)
superellipsoid.plot(color="tan")

torus = pv.ParametricTorus()
torus.plot(color="tan")

pointa = [-1, 0, 0]
pointb = [0, 1, 0]
center = [0, 0, 0]
resolution = 100

arc = pv.CircularArc(pointa, pointb, center, resolution)

pl = pv.Plotter()
pl.add_mesh(arc, color='k', line_width=4)
pl.show_bounds()
pl.view_xy()
pl.show()

pointa = [-1, 0, 0]
pointb = [1, 0, 0]
center = [0, 0, 0]
resolution = 100

arc = pv.CircularArc(pointa, pointb, center, resolution)
poly = arc.extrude([0, 0, 1])
poly.plot(color="tan", cpos='iso', show_edges=True)