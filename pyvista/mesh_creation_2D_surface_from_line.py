# %%
import pyvista as pv
from pyvista import examples
import numpy as np
import matplotlib.pyplot as plt

# %%
# Extract the data archive and load these files
# 2D array of XYZ coordinates
path = examples.download_gpr_path().points

# 2D array of the data values from the imaging equipment
data = examples.download_gpr_data_array()

# %%
fig, ax = plt.subplots(figsize=(15, 3))
ax.pcolormesh(data, cmap="seismic", clim=[-1, 1])
plt.gca().invert_yaxis()
plt.show()

# %%
fig, ax = plt.subplots()
ax.scatter(path[:, 1], path[:, 0])
ax.set_xlabel("Northing")
ax.set_ylabel("Easting")

# %%
assert len(path) in data.shape, "Make sure coordinates are present for every trace."
# If not, you'll need to interpolate the path!

# Grab the number of samples (in Z dir) and number of traces/soundings
nsamples, ntraces = data.shape # Might be opposite for your data, pay attention here

# Define the Z spacing of your 2D section
z_spacing = 0.12

# Create structured points draping down from the path
points = np.repeat(path, nsamples, axis=0)
# repeat the Z locations across
tp = np.arange(0, z_spacing*nsamples, z_spacing)
tp = path[:,2][:,None] - tp
points[:,-1] = tp.ravel()

###############################################################################
# Make a StructuredGrid from the structured points
grid = pv.StructuredGrid()
grid.points = points
grid.dimensions = nsamples, ntraces, 1

# Add the data array - note the ordering!
grid["values"] = data.ravel(order="F")

###############################################################################
# And now we can plot it! or process or do anything, because it is a PyVista
# mesh and the possibilities are endless with PyVista

cpos = [(1217002.366883762, 345363.80666238244, 3816.828857791056),
 (1216322.4753436751, 344033.0310674846, 3331.052985309526),
 (-0.17716571330686096, -0.25634368781817973, 0.9502106207279767)]

p = pv.Plotter()
p.add_mesh(grid, cmap="seismic", clim=[-1,1])
p.add_mesh(pv.PolyData(path), color='orange')
p.show(cpos=cpos)