# %%
import numpy as np
import pyvista as pv

import matplotlib.pyplot as plt

from pyvista import examples

# %%
# Make data
x = np.arange(-10, 10, 0.25)
y = np.arange(-10, 10, 0.25)
x, y = np.meshgrid(x, y)
r = np.sqrt(x ** 2 + y ** 2)
z = np.sin(r)

# Create and plot structured grid
grid = pv.StructuredGrid(x, y, z)
grid.plot()
grid.plot_curvature(clim=[-1, 1])

print(grid.points)

# %%
def make_point_set():
    n, m = 29, 32
    x = np.linspace(-200, 200, num=n) + np.random.uniform(-5, 5, size=n)
    y = np.linspace(-200, 200, num=m) + np.random.uniform(-5, 5, size=m)
    xx, yy = np.meshgrid(x, y)
    A, b = 100, 100
    zz = A * np.exp(-0.5 * ((xx / b) ** 2.0 + (yy / b) ** 2.0))
    points = np.c_[xx.reshape(-1), yy.reshape(-1), zz.reshape(-1)]
    foo = pv.PolyData(points)
    foo.rotate_z(36.6)

    return foo.points

# Get the points as a 2D NumPy array (N by 3)
points = make_point_set()
points[0:5, :]


fig, ax = plt.subplots(figsize=(10, 10))
ax.scatter(points[:, 0], points[:, 1], c=points[:, 2])
ax.set_xlabel("X Coordinate")
ax.set_ylabel("Y Coordinate")
plt.show()

# %%
mesh = pv.StructuredGrid()
mesh.points = points
mesh.dimensions = [29, 32, 1]
mesh.plot(show_edges=True, show_grid=True, cpos="xy")

# %%
struct = examples.load_structured()
struct.plot(show_edges=True)

top = struct.points.copy()
bottom = struct.points.copy()
bottom[:, -1] = -10

vol = pv.StructuredGrid()
vol.points = np.vstack((top, bottom))
vol.dimensions = [*struct.dimensions[0:2], 2]
vol.plot(show_edges=True)