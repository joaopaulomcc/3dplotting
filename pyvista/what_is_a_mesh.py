# %%
import numpy as np
import pyvista as pv


from pyvista import examples

# %%
nodes = np.random.rand(100, 3)
mesh = pv.PolyData(nodes)
mesh.plot(point_size=10)

# %%
mesh = examples.load_hexbeam()
bcpos = [(6.20, 3.00, 7.50), (0.16, 0.13, 2.65), (-0.28, 0.94, -0.21)]

p = pv.Plotter()
p.add_mesh(mesh, show_edges=True, color="white")
p.add_mesh(
    pv.PolyData(mesh.points), color="red", point_size=10, render_points_as_spheres=True
)
p.camera_position = bcpos
p.show()

# %%
mesh = examples.download_bunny_coarse()

p = pv.Plotter()
p.add_mesh(mesh, show_edges=True, color="white")
p.add_mesh(
    pv.PolyData(mesh.points), color="red", point_size=10, render_points_as_spheres=True
)
p.camera_position = [(0.02, 0.30, 0.73), (0.02, 0.03, -0.022), (-0.03, 0.94, -0.34)]

p.show()

# %%
mesh = examples.load_hexbeam()

p = pv.Plotter()
p.add_mesh(mesh, show_edges=True, color="white")
p.add_mesh(
    pv.PolyData(mesh.points), color="red", point_size=10, render_points_as_spheres=True
)
p.add_mesh(
    mesh.extract_cells(mesh.n_cells - 1),
    color="pink",
    edge_color="blue",
    line_width=5,
    show_edges=True,
)
p.camera_position = [(6.20, 3.00, 7.50), (0.16, 0.13, 2.65), (-0.28, 0.94, -0.21)]
p.show()

# %%
mesh = examples.load_hexbeam()
bcpos = [(6.20, 3.00, 7.50), (0.16, 0.13, 2.65), (-0.28, 0.94, -0.21)]

mesh.point_arrays["My point values"] = np.arange(mesh.n_points)
mesh.plot(scalars="My point values", cpos=bcpos, show_edges=True)

mesh.cell_arrays["My cell values"] = np.arange(mesh.n_cells)
mesh.plot(scalars="My cell values", cpos=bcpos, show_edges=True)

# %%
mesh = examples.load_uniform()

p = pv.Plotter(shape=(1, 2))
p.add_mesh(mesh, scalars="Spatial Point Data", show_edges=True)
p.subplot(0, 1)
p.add_mesh(mesh, scalars="Spatial Cell Data", show_edges=True)
p.show(screenshot="point_vs_cell_data.png")
