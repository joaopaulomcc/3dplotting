# %%
import numpy as np
import pyvista as pv
from pyvista import examples

pv.set_plot_theme("document")
# %%
def generate_points(subset=0.02):

    dataset = examples.download_lidar()
    ids = np.random.randint(
        low=0, high=dataset.n_points - 1, size=int(dataset.n_points * subset)
    )

    return dataset.points[ids]


points = generate_points(subset=0.05)

points[0:5, :]

# %%
point_cloud = pv.PolyData(points)
point_cloud

# %%
np.allclose(points, point_cloud.points)

# %%
point_cloud.plot(eye_dome_lighting=True)
# %%
data = points[:, 2]
point_cloud["elevation"] = data
point_cloud.plot(render_points_as_spheres=True)

# %%

points = np.random.rand(100, 3)
point_cloud = pv.PolyData(points)


def compute_vectors(mesh):
    origin = mesh.center
    vectors = mesh.points - origin
    vectors = vectors / np.linalg.norm(vectors, axis=1)[:, None]
    return vectors


vectors = compute_vectors(point_cloud)
vectors[0:5, :]
point_cloud["vectors"] = vectors

# %%

arrows = point_cloud.glyph(orient="vectors", scale=False, factor=0.15)
plotter = pv.Plotter()

plotter.add_mesh(
    point_cloud, color="maroon", point_size=10, render_points_as_spheres=True
)
plotter.add_mesh(arrows, color="lightblue")
plotter.show_grid()
plotter.show()
